import { describe, it, expect } from "vitest";
import { mount } from "@vue/test-utils";
import Form from "components/Results/Form.vue";

describe("Form", () => {
  const props = {
    data: {
      sepalLength: 5.3,
      sepalWidth: 2,
      petalLength: 2,
      petalWidth: 1.1,
    },
  };
  it("button-send-test - Emitted correctly", async () => {
    const wrapper = mount(Form, { props });
    await wrapper
      .find('[data-test="button-send-test"]')
      .trigger("click");
    expect(wrapper.emitted()).toHaveProperty("emit-test");
  });
  it("button-send-test - Emitted correctly value ", async () => {
    const wrapper = mount(Form, { props });
    wrapper.find('[data-test="button-send-test"]').trigger("click");
    const value = wrapper.emitted("emit-test");
    expect(value).toHaveLength(1)
    expect(value[0]).toEqual([props.data])
  });

  it("button-send-sample - Emitted correctly", async () => {
    const wrapper = mount(Form, { props });
    await wrapper
      .find('[data-test="button-send-sample"]')
      .trigger("click");
    expect(wrapper.emitted()).toHaveProperty("emit-send-sample");
  });
  it("button-send-sample - Emitted correctly value", async () => {
    const wrapper = mount(Form, { props });
    wrapper.find('[data-test="button-send-sample"]').trigger("click");
    const value = wrapper.emitted("emit-send-sample");
    expect(value).toHaveLength(1)
    expect(value[0]).toEqual([props.data])
  });

  it("button-send-random-sample - Emitted correctly ", async () => {
    const wrapper = mount(Form, { props });
    await wrapper
      .find('[data-test="button-send-random-sample"]')
      .trigger("click");
    expect(wrapper.emitted()).toHaveProperty("emit-send-sample");
  });
  it("button-send-random-sample - Emitted correctly value", async () => {
    const wrapper = mount(Form, { props });
    wrapper.find('[data-test="button-send-random-sample"]').trigger("click");
    const value = wrapper.emitted("emit-send-sample");
    expect(value).toHaveLength(1)
    expect(value[0]).toEqual([props.data,true])
  });
});
