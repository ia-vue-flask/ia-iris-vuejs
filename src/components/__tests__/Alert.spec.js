import { describe, it, expect } from "vitest";
import { mount } from "@vue/test-utils";
import Alert from "../Alert.vue";
import {
  STATUS_API_ERROR_TEXT
} from "@/types/model";

describe("Alert", () => {
  it("Should render correctly without slot", () => {
    const wrapper = mount(Alert);
    expect(wrapper.text()).toBe("Verificando estado...");
  });

  it("Should to have the correct class", () => {
    const props = { type: 'error' }
    const wrapper = mount(Alert, { props });
    expect(wrapper.classes()).toContain(`alert-${props.type}`);
  });
  it("Should render correctly with slot", () => {
    const slots = {
      'alert-slot': STATUS_API_ERROR_TEXT
    }
    const wrapper = mount(Alert, { slots });
    expect(wrapper.html()).toContain(STATUS_API_ERROR_TEXT);
  });
});
