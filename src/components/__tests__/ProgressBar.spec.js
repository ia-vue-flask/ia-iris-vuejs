import { describe, it, expect } from "vitest";
import { mount } from "@vue/test-utils";
import ProgressBar from "components/ProgressBar.vue";

describe("ProgressBar", () => {
  it("show component", () => {
    const wrapper = mount(ProgressBar);
    expect(wrapper.get('[data-test-value="prograssBar"]').isVisible()).toBe(
      true
    );
  });
});
