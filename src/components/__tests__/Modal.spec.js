import { describe, it, expect } from "vitest";
import { mount } from "@vue/test-utils";
import Modal from "components/Modal.vue";

describe("Modal", () => {
  it("Render correctly by default", () => {
    const wrapper = mount(Modal);
    const valueButton = wrapper.get(".modal-footer button").text();
    expect(wrapper.get("h1").text()).toContain("Título");
    expect(valueButton).toBe("Entendido");
  });

  it("Render correctly with params", () => {
    const props = {
      title: "Instrucciones",
      buttonOptions: {
        text: "Cerrar",
        class: "btn-info",
      },
    };
    const wrapper = mount(Modal, { props });
    const valueButton = wrapper.get(".modal-footer button").text();

    expect(wrapper.get("h1").text()).toContain("Instrucciones");
    expect(valueButton).toBe("Cerrar");
    expect();
  });

  it("Render correctly with slot", () => {
    const slots = {
      content: "Slot content",
    };
    const wrapper = mount(Modal, { slots });
    expect(wrapper.html()).toContain("Slot content");
  });

  it("Visibility false by default", async () => {
    const wrapper = mount(Modal);
    const displayModal = wrapper.get("#exampleModal");
    expect(displayModal.classes('show')).toBe(false)
  });
});
