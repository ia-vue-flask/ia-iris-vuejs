import { defineStore } from "pinia";
import { verifyHealthcheckAPI } from "services/index.js";
import { STATUS_API_SUCCESS, STATUS_API_ERROR, STATUS_API_VERIFY } from "@/types/model";
const useCoreStore = defineStore("core", {
  state: () => ({ statusAPI: STATUS_API_VERIFY, loading: false }),
  getters: {
    getStateAPI: (state) => state.statusAPI,
  },
  actions: {
    async getHealthcheck() {
      this.loading = true
      try {
        const response = await verifyHealthcheckAPI();
        if (response.status !== 200) this.statusAPI = STATUS_API_ERROR;
        this.statusAPI = STATUS_API_SUCCESS;
      } catch (error) {
        console.log("error  getting health:>> ", error);
        this.statusAPI = STATUS_API_ERROR
      } finally {
        this.loading = false;
      }
    },
  },
});

export { useCoreStore };
