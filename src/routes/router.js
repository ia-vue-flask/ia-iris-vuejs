import { createWebHistory, createRouter } from "vue-router";
import HomeView from "views/HomeView.vue";
import Results from "views/Results.vue";
import { app } from "@/config";

const routes = [
  { path: `/`, name: "home", component: HomeView },
  {
    path: `/results`,
    name: "results",
    component: Results,
  },
  { path: `/:pathMatch(.*)*`, redirect: { name: "home" } },
];
const router = createRouter({
  linkActiveClass: "link-primary",
  history: createWebHistory(app.baseURL),
  routes,
});

export default router;
