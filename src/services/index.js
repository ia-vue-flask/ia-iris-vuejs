import axios from "../../axios.config";

const verifyHealthcheckAPI = () => {
  return axios.get("/healthcheck");
};

const getPredictTreeClassifier = (data) => {
  return axios.get("/tree_classifier", {
    params: data,
  });
};

const getPredictSVC = (data) => {
  return axios.get("/svc", {
    params: data,
  });
};

export { verifyHealthcheckAPI, getPredictTreeClassifier, getPredictSVC };
