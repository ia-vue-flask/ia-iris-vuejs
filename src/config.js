const { VITE_APP_BASE_API, VITE_APP_BASE_URL } = import.meta.env;

const axiosConfig = {
  baseURL: VITE_APP_BASE_API,
};
const app = {
  baseURL: VITE_APP_BASE_URL || '/'
}

export { axiosConfig, app };
