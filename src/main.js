import { createApp } from "vue";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Axios from "../axios.config";
import { createPinia } from "pinia";
import router from "./routes/router";
import App from "./App.vue";

const pinia = createPinia();
const app = createApp(App);

app.config.globalProperties.$axios = Axios;

app.use(pinia);
app.use(router);
app.mount("#app");
