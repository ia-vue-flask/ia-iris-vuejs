const ALL_RESULTS_FILTER = "all";
const WRONG_RESULTS_FILTER = "wrong";
const SUCCESS_RESULTS_FILTER = "success";

const TEST_ACTION = "test";
const RANDOM_ACTION = "random";

const STATUS_API_SUCCESS = "success";
const STATUS_API_ERROR = "error";
const STATUS_API_VERIFY = "verify";

const STATUS_API_SUCCESS_TEXT = "Activa";
const STATUS_API_ERROR_TEXT = "Lo sentimos, en este momento estamos verificando qué sucede con la API";
const STATUS_API_VERIFY_TEXT = "Activando";

export {
  ALL_RESULTS_FILTER,
  WRONG_RESULTS_FILTER,
  SUCCESS_RESULTS_FILTER,
  TEST_ACTION,
  RANDOM_ACTION,
  STATUS_API_SUCCESS,
  STATUS_API_ERROR,
  STATUS_API_VERIFY,
  STATUS_API_SUCCESS_TEXT,
  STATUS_API_ERROR_TEXT,
  STATUS_API_VERIFY_TEXT,
};
