# IA - Iris

Aplicación de IA que implementa modelos de árbol de decisiones y SVC para clasificar la flor iris a partir de muestras aleatorias, permitiendo verificar predicciones vs etiquetas reales.

## Engine

Use node v18.20.3, Vue 3.4 and Vite


## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

# Docker
## Create image
Lo que se debe enviar al container es el /dist, por lo que debo crear una carpeta que contenga el dockerfile y el dist, luego en esa carpeta ejecuto: 
```
docker build -t app_iris_image .
```
## Create container
```
docker run -it -p 8080:80 --name iris_app_container -d app_iris_image
```


[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)