# specify the node base image with your desired version node:<version>
FROM node:18.20.4
RUN npm install -g http-server
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
EXPOSE 80
CMD [ "http-server", "dist" ]