import axios from "axios";
import { axiosConfig } from "./src/config";
const instance = axios.create({
    "baseURL": axiosConfig.baseURL
})
export default instance;
