module.exports = {
  root: true,
  env: {
    node: true,
    es2022: true,
  },
  extends: [
    'plugin:vue/essential',
    'plugin:vue/strongly-recommended',
    'plugin:vue/recommended',
    'prettier'
  ],
  parserOptions: {
  },
  rules: {
    "vue/multi-word-component-names" : "off"
  }
}
