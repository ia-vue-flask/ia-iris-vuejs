import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  base: process.env.NODE_ENV === 'production'
  ? '/' + (process.env.CI_PROJECT_NAME ? process.env.CI_PROJECT_NAME + '/' : '') 
  : '/',
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      'components': fileURLToPath(new URL('./src/components', import.meta.url)),
      'views': fileURLToPath(new URL('./src/views', import.meta.url)),
      'services': fileURLToPath(new URL('./src/services', import.meta.url)),
    }
  }
  
});